# Utiliza una imagen base de Java 8 y Maven para compilar el proyecto
FROM maven:3.8.4-jdk-8 AS build

# Crea un directorio de trabajo en el contenedor
WORKDIR /app

# Copia el archivo pom.xml y los archivos fuente al contenedor
COPY pom.xml .
COPY src ./src

# Compila el proyecto y empaqueta el JAR
RUN mvn package

# Utiliza una imagen base de Java 8
FROM openjdk:8-jre-alpine

# Crea un directorio de trabajo en el contenedor
WORKDIR /app

# Copia el archivo JAR desde la imagen de compilación al directorio de trabajo en el contenedor
COPY --from=build /app/target/TareaIsoft-2023-1.0-SNAPSHOT.jar .

# Comando para ejecutar el archivo JAR
CMD ["java", "-jar", "TareaIsoft-2023-1.0-SNAPSHOT.jar"]
