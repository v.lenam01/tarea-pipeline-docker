package org.example;

public class Main {
    public static void main(String[] args) {
        Task task1 = new Task("Hacer la tarea de matemáticas", "Resolver los problemas del capítulo 5");
        Task task2 = new Task("Comprar víveres", "Huevos, leche, pan, frutas");

        TaskList taskList = new TaskList();
        taskList.addTask(task1);
        taskList.addTask(task2);



        // Marcar una tarea como completada
        task1.setCompleted(true);


        //Tasklist agregar tarea a la lista
        System.out.println("se agregan tareas a la lista");

 }
}
